#include <stdio.h>
#include <stdlib.h>

#include<windows.h>

#include <C:/GLUT/include/GL/glut.h>

float angulo=3.1;
float lx=0.041f,lz=1.0f;
float x=21.0f,z=-6.0f, y=7;
float aux1=0.0;
float aux2=0.0;
int xint=0,zint=0;
float grosorLoza=.5;
float i = 0;
float j = 0;
int k = 0;

GLuint _piso;
GLuint _piso2;
GLint _cielo;
GLint _bola;
GLint _blanco;
GLint _rojo;
GLint _pantalla;
GLint _techo;
GLint _pared;
GLint _negro;
GLUquadricObj *sphere;

int velocidad = 1;
bool efectoderecha = false;
bool efectoizquierda = false;
int cantidaddepinos = 10;
int bolax = 7;
int bolay = 0;
int oportunidad = 0;

char tablero[30][15] = {

{ 'c','0','0','0','0','0','0','b','0','0','0','0','0','0','c' }, //0
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //1
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //2
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //3
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //4
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //5
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //6
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //7
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //8
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //9
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //10
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //11
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //12
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //13
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //14
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //15
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //16
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //17
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //18
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //19
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //20
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //21
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //22
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //23
{ 'c','0','0','0','0','0','0','p','0','0','0','0','0','0','c' }, //24
{ 'c','0','0','0','0','0','p','0','p','0','0','0','0','0','c' }, //25
{ 'c','0','0','0','0','p','0','p','0','p','0','0','0','0','c' }, //26
{ 'c','0','0','0','p','0','p','0','p','0','p','0','0','0','c' }, //27
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //28
{ 'c','0','0','0','0','0','0','0','0','0','0','0','0','0','c' }, //29
};


int pino1x = 4,pino1y=27;
int pino2x = 5,pino2y=26;
int pino3x = 6,pino3y=25;
int pino4x = 6,pino4y=27;
int pino5x = 7,pino5y=26;
int pino6x = 7,pino6y=24;
int pino7x = 8,pino7y=27;
int pino8x = 8,pino8y=25;
int pino9x = 9,pino9y=26;
int pino10x = 10,pino10y=27;

void draw_circle(){
    sphere = gluNewQuadric();
    gluQuadricTexture(sphere, GL_TRUE);
    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _bola);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluSphere(sphere,2,100,100);
}

void circulo(){
    sphere = gluNewQuadric();
    gluQuadricTexture(sphere, GL_TRUE);
    glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _cielo);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gluSphere(sphere,110,100,100);
}

void initRendering() {
	Image* blanco= loadBMP("C://Users//skape//OneDrive//Desktop//yaret//boliche-master//Boliche//blanco.bmp");
	Image* rojo= loadBMP("C://Users//skape//OneDrive//Desktop//yaret//boliche-master//Boliche//rojo.bmp");
	_blanco = loadTexture(blanco);
	_rojo = loadTexture(rojo);
	delete blanco,rojo;
}

void display(void) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
	gluLookAt( x, y+aux1,z,x+lx,y+aux2,z+lz,0.0f, y, 0.0f);
	glPushMatrix();
	circulo();
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(00,00);
	glutInitWindowSize(800,600);
	glutCreateWindow("Casa");
	glutDisplayFunc(display);
	glutIdleFunc(display);
	glEnable(GL_DEPTH_TEST);
	glutMainLoop();
	return 0;
}
